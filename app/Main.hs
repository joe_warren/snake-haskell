{-# LANGUAGE OverloadedStrings #-} 
{-# LANGUAGE GeneralizedNewtypeDeriving #-} 

module Main where

import qualified Brick as B
import qualified Brick.BChan as BChan
import qualified Brick.Widgets.Border as Border
import qualified Brick.Widgets.Center as Center
import qualified Brick.Widgets.Border.Style as BorderStyle
import qualified Graphics.Vty as V
import Control.Lens ((<&>))
import Control.Monad
import Data.Map (Map)
import qualified Data.Map as M
import Linear.V2 (V2(..))
import Control.Concurrent (threadDelay, forkIO)

-- | Ticks mark passing of time
data Tick = Tick

-- | Named resources
type Name = ()

stackedLines :: B.Widget Name
stackedLines = foldr (B.<=>) B.emptyWidget []


data Direction = North | East | South | West deriving Eq 

opposite :: Direction -> Direction
opposite North = South
opposite East = West
opposite South = North
opposite West = East

newtype  Block = Block { getPos :: (Int, Int) } deriving Eq

data Snake = Snake {
    getDirection :: Direction,
    previousDirection :: Direction, 
    getBlocks :: [Block]
}

data Fruit = Fruit {
    fruitBlock :: Block, 
    effect :: Game -> Game, 
    ambientEffect :: Game -> Game,
    fruitName :: String
}

data Game = GameOver {score ::Int} | Game {
    score :: Int,
    gameHeight :: Int, 
    gameWidth :: Int,
    fruit :: [Fruit],
    snake :: Snake
}

initialSnake :: Snake
initialSnake = Snake { 
             getBlocks=replicate 5 $ Block (5, 5),
             getDirection=North,
             previousDirection=North
        }

initialFruit = Fruit (Block (10,10)) id id "apple" 

initialGame :: Game 
initialGame = Game 0 12 24 [initialFruit] initialSnake

drawUI :: Game -> [B.Widget Name]
drawUI g@GameOver{score=s} = [Center.hCenter $ Center.vCenter $
             B.withBorderStyle BorderStyle.unicodeRounded $
             Border.borderWithLabel  
    (B.str $ "Score" ) $ 
        foldr (B.<+>) B.emptyWidget [B.padLeftRight 10 $ B.str $ show s]]
drawUI g =
  [Center.hCenter $ Center.vCenter $ B.vLimit 22 $ B.withBorderStyle BorderStyle.unicodeRounded $
  Border.borderWithLabel
    (B.str $ show $ score g) $ 
    B.vBox $ [gameHeight g, gameHeight g - 1 .. 1] <&> \r ->
          foldr (B.<+>) B.emptyWidget
          . M.filterWithKey (\(V2 _ y) _ -> r == y)
            $  mconcat [ snakeMap g, fruitMap g, emptyCellMap g ] 
            ]


emptyAttr :: B.AttrName
emptyAttr = "empty"

snakeAttr :: B.AttrName
snakeAttr = "snake"

fruitAttr :: B.AttrName
fruitAttr = "fruit"

fruitMap :: Game -> Map (V2 Int) (B.Widget Name)
fruitMap g = M.fromList [ ((V2 x y), ew) | (x, y) <- getPos <$> (fruitBlock <$> (fruit $ g))]
  where 
    ew = B.withAttr fruitAttr (B.str $ "◖◗")

--snakeTxt :: Int -> String
--snakeTxt 0 = "▟▛"
--snakeTxt 1 = " ▟"
--snakeTxt 2 = "▛ " 

snakeMap :: Game -> Map (V2 Int) (B.Widget Name)
snakeMap g = M.fromList [ ((V2 x y), ew x y) | (x, y) <- getPos <$> (getBlocks $ snake $ g)]
  where 
    ew x y  = B.withAttr snakeAttr (B.str $ snakeTxt ((x + y) `mod` 3))
    snakeTxt 0 = "▟▛"
    snakeTxt 1 = " ▟"
    snakeTxt 2 = "▛ " 

emptyCellMap :: Game -> Map (V2 Int) (B.Widget Name)
emptyCellMap g = M.fromList [ ((V2 x y), ew x y) | x <- [1 .. gameWidth g], y <- [1 .. gameHeight g] ]
  where
    ew x y = B.withAttr emptyAttr (B.str $ s x y)
    s x y = if 0 == y`mod`2 then " ." else ". "

progressHead :: Direction -> Block -> Block
progressHead North (Block (x, y)) = Block (x, y+1)
progressHead South (Block (x, y)) = Block (x, y-1)
progressHead East (Block (x, y)) = Block (x-1, y)
progressHead West (Block (x, y)) = Block (x+1, y)

progressSnakeBlocks :: Direction -> [Block] -> [Block]
progressSnakeBlocks d (h:t:[]) = (progressHead d h) : h : []
progressSnakeBlocks d (h:ts) = (progressHead d h) : h : init ts

progressSnake :: Snake -> Snake
progressSnake s = s {previousDirection = d, getBlocks = progressSnakeBlocks d b}
  where 
    d = getDirection s
    b = getBlocks s

checkDeath :: Game -> Game
checkDeath g = if safe then g else GameOver $ score g
  where
    blocks = getBlocks $ snake g
    safeX1 = not $ any (\x -> fst x <= 0) $ getPos <$> blocks
    safeY1 = not $ any (\y -> snd y <= 0) $ getPos <$> blocks
    safeX2 = not $ any (\x -> fst x > gameWidth g) $ getPos <$> blocks
    safeY2 = not $ any (\y -> snd y > gameHeight g) $ getPos <$> blocks
    safeBite = not $ any ((==) (head blocks)) $ tail blocks  
    safe = safeX1 && safeY1 && safeX2 && safeY2 && safeBite

progress :: Game -> Game
progress g@GameOver{} = g
progress g = checkDeath $ g {score=1 + score g, snake = progressSnake $ snake g}

keyPress :: Game -> Direction -> Game
keyPress g d | d /= (opposite $ previousDirection $ snake g) = g {snake = (snake g){ getDirection = d }}
keyPress g _ = g

handleEvent :: Game -> B.BrickEvent Name Tick -> B.EventM Name (B.Next Game)
handleEvent ui (B.VtyEvent (V.EvKey (V.KChar 'q') [])) = B.halt ui
handleEvent ui (B.VtyEvent (V.EvKey V.KEsc        [])) = B.halt ui
handleEvent g (B.VtyEvent (V.EvKey V.KUp [])) = B.continue (keyPress g North)
handleEvent g (B.VtyEvent (V.EvKey V.KDown [])) = B.continue (keyPress g South)
handleEvent g (B.VtyEvent (V.EvKey V.KLeft [])) = B.continue (keyPress g East)
handleEvent g (B.VtyEvent (V.EvKey V.KRight [])) = B.continue (keyPress g West)
handleEvent g (B.AppEvent Tick) = B.continue (progress g)
handleEvent g _ = B.continue g

theAttrMap :: B.AttrMap
theAttrMap = B.attrMap V.defAttr [
        (emptyAttr, V.green `B.on` V.rgbColor 255 255 255), 
        (snakeAttr, V.brightRed   `B.on` V.black)
    ]


app :: B.App Game Tick Name
app = B.App
  { B.appDraw         = drawUI
  , B.appChooseCursor = B.neverShowCursor
  , B.appHandleEvent  = handleEvent
  , B.appStartEvent   = return
  , B.appAttrMap      = const theAttrMap
  }

main :: IO Game
main = do
    chan <- BChan.newBChan 10
    void . forkIO $ forever $ do
      BChan.writeBChan chan Tick
      threadDelay 1000000
    B.customMain (V.mkVty V.defaultConfig) (Just chan) app initialGame

